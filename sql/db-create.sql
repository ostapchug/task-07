/* DROP DATABASE IF EXISTS testdb; */

/* CREATE DATABASE testdb; */

USE testdb;

DROP TABLE IF EXISTS `users_teams` CASCADE;
DROP TABLE IF EXISTS `users` CASCADE;
DROP TABLE IF EXISTS `teams` CASCADE;

CREATE TABLE users (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`login` VARCHAR(10) NOT NULL,
    CONSTRAINT `PK_users` PRIMARY KEY (`id` ASC)
);

CREATE TABLE teams (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(10) NOT NULL,
    CONSTRAINT `PK_teams` PRIMARY KEY (`id` ASC)
);

CREATE TABLE users_teams (
	user_id INT UNSIGNED NOT NULL,
	team_id INT UNSIGNED NOT NULL,
	CONSTRAINT `PK_users_teams` PRIMARY KEY (`user_id` ASC, `team_id` ASC)
);

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE `users`
 ADD CONSTRAINT `UQ_users_login` UNIQUE (`login` ASC)
;

ALTER TABLE `teams`
 ADD CONSTRAINT `UQ_teams_name` UNIQUE (`name` ASC)
;

/* Create Foreign Key Constraints */

ALTER TABLE `users_teams`
 ADD CONSTRAINT `FK_users_teams_users`
	FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE Cascade ON UPDATE Cascade
;

ALTER TABLE `users_teams`
 ADD CONSTRAINT `FK_users_teams_teams`
	FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE Cascade ON UPDATE Cascade
;

INSERT INTO users VALUES (DEFAULT, 'ivanov');
INSERT INTO teams VALUES (DEFAULT, 'teamA');