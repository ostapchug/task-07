package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private String url;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try (InputStream input = new FileInputStream("app.properties")) {
			Properties props = new Properties();
			props.load(input);
			url = props.getProperty("connection.url");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	private Connection getConnection() throws DBException{
		Connection connection;
		try {
			connection = DriverManager.getConnection(url);
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return connection;
	}

	public List<User> findAllUsers() throws DBException {
		String sql = "SELECT id, login FROM users";
		List<User> users = new ArrayList<>();

		try (Connection connection = getConnection();
			 Statement statement = connection.createStatement();
			 ResultSet resultSet = statement.executeQuery(sql))
		{
			while (resultSet.next()){
				User user = new User();
				user.setId(resultSet.getInt(1));
				user.setLogin(resultSet.getString(2));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
		String sql = "INSERT INTO users (id, login) VALUES (DEFAULT, ?)";
		boolean result;

		try (Connection connection = getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS))
		{
			preparedStatement.setString(1, user.getLogin());
			int n = preparedStatement.executeUpdate();
			result = n > 0;

			try(ResultSet resultSet = preparedStatement.getGeneratedKeys()){
				while (resultSet.next()){
					user.setId(resultSet.getInt(1));
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return result;
	}

	public boolean deleteUsers(User... users) throws DBException {
		String sql = "DELETE FROM users WHERE login = ?";
		boolean result;

		try (Connection connection = getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(sql))
		{
			int n = 0;
			for (User user : users){
				preparedStatement.setString(1, user.getLogin());
				if (preparedStatement.executeUpdate() > 0){
					n++;
				}
			}

			result = n == users.length;

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return result;
	}

	public User getUser(String login) throws DBException {
		String sql = "SELECT id, login FROM users WHERE login = ?";
		User user = null;
		try (Connection connection = getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(sql))
		{
			preparedStatement.setString(1, login);

			try(ResultSet resultSet = preparedStatement.executeQuery()){
				while (resultSet.next()){
					user = new User();
					user.setId(resultSet.getInt(1));
					user.setLogin(resultSet.getString(2));
				}
			}
		}catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		String sql = "SELECT id, name FROM teams WHERE name = ?";
		Team team = null;
		try (Connection connection = getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(sql))
		{
			preparedStatement.setString(1, name);

			try(ResultSet resultSet = preparedStatement.executeQuery()){
				while (resultSet.next()){
					team = new Team();
					team.setId(resultSet.getInt(1));
					team.setName(resultSet.getString(2));
				}
			}
		}catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return team;

	}

	public List<Team> findAllTeams() throws DBException {
		String sql = "SELECT id, name FROM teams";
		List<Team> teams = new ArrayList<>();

		try (Connection connection = getConnection();
			 Statement statement = connection.createStatement();
			 ResultSet resultSet = statement.executeQuery(sql))
		{
			while (resultSet.next()){
				Team team = new Team();
				team.setId(resultSet.getInt(1));
				team.setName(resultSet.getString(2));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		String sql = "INSERT INTO teams (id, name) VALUES (DEFAULT, ?)";
		boolean result;

		try (Connection connection = getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS))
		{
			preparedStatement.setString(1, team.getName());
			int n = preparedStatement.executeUpdate();
			result = n > 0;

			try(ResultSet resultSet = preparedStatement.getGeneratedKeys()){
				while (resultSet.next()){
					team.setId(resultSet.getInt(1));
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return result;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String sql = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";

		try (Connection connection = getConnection()) {
				try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
					connection.setAutoCommit(false);
					for (Team team : teams){
						preparedStatement.setInt(1, user.getId());
						preparedStatement.setInt(2, team.getId());
						preparedStatement.executeUpdate();
					}
					connection.commit();
				} catch (SQLException e){
					connection.rollback();
					throw e;
				}

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		String sql = "SELECT id, name FROM teams, users_teams WHERE team_id = id AND user_id = ?";
		List<Team> teams = new ArrayList<>();

		try (Connection connection = getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(sql))
		{
			preparedStatement.setInt(1, getUser(user.getLogin()).getId());

			try (ResultSet resultSet = preparedStatement.executeQuery()){
				while (resultSet.next()){
					Team team = new Team();
					team.setId(resultSet.getInt(1));
					team.setName(resultSet.getString(2));
					teams.add(team);
				}
			}

		}catch (SQLException e) {
			throw new DBException(e.getMessage(), e);

		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String sql = "DELETE FROM teams WHERE name = ?";
		boolean result;

		try (Connection connection = getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(sql))
		{
			preparedStatement.setString(1, team.getName());
			int n = preparedStatement.executeUpdate();
			result = n > 0;
		}catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return result;
	}

	public boolean updateTeam(Team team) throws DBException {
		String sql = "UPDATE teams SET name = ? WHERE id = ?";
		boolean result;

		try (Connection connection = getConnection();
			 PreparedStatement preparedStatement = connection.prepareStatement(sql))
		{
			preparedStatement.setString(1, team.getName());
			preparedStatement.setInt(2, team.getId());
			int n = preparedStatement.executeUpdate();
			result = n > 0;
		}catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return result;
	}

}
